Ray Amouzandeh and his team at TARGA Residential represent many of the best apartments,condominiums, and rental homes throughout the city, specializing in sought-after communities such as the Mission District, Noe Valley, Castro, Bernal Heights, Nob Hill, North Beach and SOMA.

Address: 1390 Market St, suite 200, San Francisco, CA 94102, USA

Phone: 415-494-7009
